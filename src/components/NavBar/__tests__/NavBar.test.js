import { mount } from "@vue/test-utils";
import NavBar from "../../NavBar";
import store from "@/store";

describe("NavBar", () => {
  it("renders and matches snapshot", () => {
    const wrapper = mount(NavBar, {
      propsData: {
        isBurgerOpen: false,
        openMenu: () => {},
        closeMenu: () => {}
      },
      stubs: ["MenuIcon", "CloseIcon", "router-link"],
      store
    });

    expect(wrapper.element).toMatchSnapshot();
    expect(wrapper.exists()).toBe(true);
  });
});
