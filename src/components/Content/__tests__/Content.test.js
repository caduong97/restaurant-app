import { mount } from "@vue/test-utils";
import Content from "../../Content";

describe("Content", () => {
  it("renders and matches snapshot", () => {
    const wrapper = mount(Content, {
      propsData: {
        heading: "testing heading"
      }
    });

    expect(wrapper.element).toMatchSnapshot();
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.contains("h1")).toBe(true);
  });

  it("has a paragraph and slot wrapper", () => {
    const testParagraph = "Lorem Ipsum";
    const wrapper = mount(Content, {
      propsData: {
        heading: "heading",
        paragraph: testParagraph,
        isButton: true
      }
    });

    const paragraph = wrapper.find("p");
    const buttonsDiv = wrapper.find(".content__buttons");

    expect(paragraph.is("p")).toBe(true);
    expect(buttonsDiv.classes()).toContain("content__buttons");
  });
});
