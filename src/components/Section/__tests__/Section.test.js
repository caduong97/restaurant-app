import { mount } from "@vue/test-utils";
import Section from "../../Section";

describe("Section", () => {
  it("renders and matches snapshot", () => {
    const wrapper = mount(Section, {
      propsData: {
        bgColor: "dark"
      }
    });

    expect(wrapper.element).toMatchSnapshot();
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.classes()).toContain("section--dark");
  });
});
