import { mount } from "@vue/test-utils";
import Footer from "../../Footer";

describe("Footer", () => {
  it("renders and matches snapshot", () => {
    const wrapper = mount(Footer, {
      stubs: ["Grid", "router-link"]
    });
    const childDiv = wrapper.find(".footer__links");

    expect(wrapper.element).toMatchSnapshot();
    expect(wrapper.exists()).toBe(true);
    expect(childDiv.classes()).toContain("footer__links");
  });
});
