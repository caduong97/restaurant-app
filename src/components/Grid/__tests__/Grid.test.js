import { mount } from "@vue/test-utils";
import Grid from "../../Grid";

describe("Grid", () => {
  it("renders and matches snapshot", () => {
    const wrapper = mount(Grid, {
      propsData: {
        colNum: 3
      }
    });

    expect(wrapper.element).toMatchSnapshot();
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.classes()).toContain("grid--3");
  });
});
