import { mount } from "@vue/test-utils";
import MenuItem from "../../MenuItem";

describe("MenuItem", () => {
  const testItem = {
    id: 1,
    name: "test",
    price: 123,
    description: "description"
  };
  it("renders and matches snapshot", () => {
    const wrapper = mount(MenuItem, {
      propsData: {
        item: testItem
      }
    });

    expect(wrapper.element).toMatchSnapshot();
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.classes()).toContain("item");
  });
});
