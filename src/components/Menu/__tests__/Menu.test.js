import { mount } from "@vue/test-utils";
import Menu from "../../Menu";

describe("Menu", () => {
  const testMenu = [
    {
      id: 1,
      price: 7,
      name: "chả giò",
      description:
        "3- fried rolls with pork, sweet potato, homemade carrot – daikon, mushroom and onion served with fish sauce"
    },
    {
      id: 2,
      price: 7,
      name: "gỏi cuốn",
      description:
        "2-fresh rolls with rice vermicelli, salad, cucumber, mint, cilantro, homemade carrot – daikon pickle and hoisin peanut sauce"
    },
    {
      id: 3,
      price: 10,
      name: "BEEF/CHICKEN SALAD",
      description: "Beef or Chicken salad with mixed greens"
    }
  ];
  it("renders and matches snapshots", () => {
    const wrapper = mount(Menu, {
      propsData: {
        type: "appertizers",
        menu: testMenu
      }
    });

    expect(wrapper.element).toMatchSnapshot();
    expect(wrapper.exists()).toBe(true);
  });
});
