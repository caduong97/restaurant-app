import { mount } from "@vue/test-utils";
import OrderItem from "../../OrderItem";
import store from "@/store";

describe("OrderItem", () => {
  const testItem = {
    id: 1,
    name: "test",
    price: 123,
    description: "description"
  };
  it("renders and matches snapshot", () => {
    const wrapper = mount(OrderItem, {
      propsData: {
        item: testItem
      },
      store
    });

    expect(wrapper.element).toMatchSnapshot();
    expect(wrapper.exists()).toBe(true);
  });

  it("has close button that fires action", () => {
    const closeMock = jest.fn();
    const wrapper = mount(OrderItem, {
      propsData: {
        item: testItem
      },
      store
    });

    wrapper.setMethods({ onClick: closeMock });

    const closeButton = wrapper.find(".order-item__remove");
    closeButton.trigger("click");
    expect(closeMock).toHaveBeenCalled();
  });
});
