import { mount } from "@vue/test-utils";
import Review from "../../Review";

describe("Review", () => {
  it("renders and matches snapshot", () => {
    const wrapper = mount(Review, {
      propsData: {
        imgUrl: "logo-2.png",
        linkTo: "#",
        text: "review"
      }
    });

    expect(wrapper.element).toMatchSnapshot();
    expect(wrapper.exists()).toBe(true);
  });
});
