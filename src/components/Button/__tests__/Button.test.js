import { mount } from "@vue/test-utils";
import Button from "../../Button";

describe("Button", () => {
  it("renders and matches snapshot", () => {
    const wrapper = mount(Button, {
      propsData: {
        text: "button"
      }
    });
    expect(wrapper.element).toMatchSnapshot();
    expect(wrapper.exists()).toBe(true);
    expect(wrapper.contains("button")).toBe(true);
    expect(wrapper.props().text).toBe("button");
  });
  it("has variant props", () => {
    const wrapper = mount(Button, {
      propsData: {
        text: "button",
        variant: "dark"
      }
    });

    expect(wrapper.props().variant).toBe("dark");
    expect(wrapper.classes()).toContain("btn--dark");
  });
  it("fires onClick", () => {
    let test = false;

    const wrapper = mount(Button, {
      propsData: {
        text: "button",
        onClick: () => {
          test = true;
        }
      }
    });

    wrapper.trigger("click");
    expect(test).toBe(true);
  });

  it("act as a link button", () => {
    const wrapper = mount(Button, {
      propsData: {
        text: "button",
        externalLink: true,
        linkTo: "#"
      }
    });

    const linkTag = wrapper.find("a");

    expect(linkTag.is("a")).toBe(true);
    expect(linkTag.attributes("href")).toBe("#");
  });
});
