import Vue from "vue";
import Router from "vue-router";
import Home from "./pages/Home.vue";
import MenuOrder from "./pages/MenuOrder.vue";
import Reservation from "./pages/Reservation.vue";
import Order from "./pages/Order.vue";
import NotFound from "./pages/NotFound.vue";

Vue.use(Router);

export default new Router({
  mode: "history", //TODO: read more about this
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/menu-and-order",
      name: "menu and order",
      component: MenuOrder
    },
    {
      path: "/reservation",
      name: "reservation",
      component: Reservation
    },
    {
      path: "/your-order",
      name: "your order",
      component: Order
    },
    {
      path: "*",
      name: "notfound",
      component: NotFound
    }
  ]
});
