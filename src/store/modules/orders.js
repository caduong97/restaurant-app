const state = {
  orders: [],
  total: 0
};

const getters = {
  allOrders: state => state.orders,
  totalPrice: state => state.total
};

const actions = {
  addItem: ({ commit }, item) => {
    commit("addItem", item);
  },
  removeItem: ({ commit }, id) => {
    commit("removeItem", id);
  },
  countTotal: ({ commit }) => {
    commit("countTotal");
  }
};

const mutations = {
  addItem: (state, item) => {
    const checkItemExistence = state.orders.some(el => el.id === item.id);
    if (checkItemExistence) {
      const newState = state.orders.map(el => {
        if (item.id === el.id) {
          return {
            id: item.id,
            name: item.name,
            price: item.price,
            quantity: el.quantity + item.quantity
          };
        } else {
          return el;
        }
      });

      return (state.orders = newState);
    }

    return state.orders.push(item);
  },
  removeItem: (state, id) => {
    return (state.orders = state.orders.filter(i => i.id !== id));
  },
  countTotal: state => {
    const finalPrices =
      state.orders.length !== 0
        ? state.orders.map(el => {
            return el.price * el.quantity;
          })
        : [];

    const totalPrice = finalPrices.reduce((a, b) => a + b, 0);

    return (state.total = totalPrice);
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
