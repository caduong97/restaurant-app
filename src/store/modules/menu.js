const state = {
  //todo: find open APIs for restaurant menu
  appertizers: [
    {
      id: 1,
      price: 7,
      name: "chả giò",
      description:
        "3- fried rolls with pork, sweet potato, homemade carrot – daikon, mushroom and onion served with fish sauce"
    },
    {
      id: 2,
      price: 7,
      name: "gỏi cuốn",
      description:
        "2-fresh rolls with rice vermicelli, salad, cucumber, mint, cilantro, homemade carrot – daikon pickle and hoisin peanut sauce"
    },
    {
      id: 3,
      price: 10,
      name: "BEEF/CHICKEN SALAD",
      description: "Beef or Chicken salad with mixed greens"
    },
    {
      id: 4,
      price: 7,
      name: "LOTUS ROOT SALAD",
      description: "Lotus root, cucumber, carrot, onion, herb, peanut"
    },
    {
      id: 5,
      price: 4.5,
      name: "SPRING ROLL",
      description:
        "Shrimp and crab spring rolls served with mixed vegetables (2 pieces)"
    },
    {
      id: 6,
      price: 4.5,
      name: "POTATO SHRIMP",
      description:
        "Potato shrimps served with cucumber, pickle, cilantro and fish sauce (2 pieces)"
    }
  ],
  mains: [
    {
      id: 7,
      price: 13.8,
      name: "PHỞ SÀI GÒN",
      description:
        "Ultimate Pho noodle soup with 12-hour- beef-broth, onion, cilantro, basil and bean sprout"
    },
    {
      id: 8,
      price: 14.5,
      name: "PHỞ CHAY (V)",
      description:
        "Vegan pho noodle with soy ham, broccoli, mushroom, cilantro, onion, basil and bean sprout"
    },
    {
      id: 9,
      price: 14.5,
      name: "BÚN THỊT NƯỚNG",
      description:
        "Rice vermicelli noodle with fried roll, sweet potato fritter, homemade carrot – daikon pickle, salad, cucumber, mint, cilantro and peanut"
    },
    {
      id: 10,
      price: 14.5,
      name: "CƠM TẤM",
      description:
        "Broken rice served with grilled lemongrass pork, sunnyside up egg, cucumber, carrot – daikon pickle and fish sauce"
    },
    {
      id: 11,
      price: 15,
      name: "GÀ CHIÊN NƯỚC MẮM",
      description:
        "Fish sauce chicken served with broken rice, carrot – daikon pickle, cucumber and green salad"
    },
    {
      id: 12,
      price: 15.5,
      name: "CƠM GÀ",
      description:
        "BBQ grilled Chicken served with broken rice and fish-sauce eggplant, peanuts, carrot – daikon pickle, cucumber"
    }
  ],
  desserts: [
    {
      id: 13,
      price: 4.5,
      name: "MOCHI IN GINGER SYRUP",
      description:
        "Black sesame sticky rice dumpling in coconut milk and ginger syrup"
    },
    {
      id: 14,
      price: 5.5,
      name: "BANANA IN COCONUT MILK",
      description:
        "Vietnamese sweet soup in coconut milk, topped with crushed peanut"
    },
    {
      id: 15,
      price: 6.5,
      name: "PANCAKE WITH FRUITS",
      description: "Pancake with fruits and ice-cream"
    }
  ]
};

const getters = {
  allAppertizers: state => state.appertizers,
  allMains: state => state.mains,
  allDesserts: state => state.desserts
};

export default {
  state,
  getters
};
