# Restaurant Landing Page and Web App
## Live site is available at: https://restaurant-app-develop.netlify.com/

## Description
An imaginary restaurant's landing page that implements food ordering and table reservation functionality

## Feature
- Routing
- Choose food item to add to the order
- Table reservation form

## Technologies
- VueJS (initialized with Vue CLI)
- Vuex for state management
- Jest as test runner
- SCSS for styling
- Netlify (Netlify CLI) hosting service

## Project setup
- Clone the project
``` 
git clone https://gitlab.com/caduong97/restaurant-app.git 
```
- Install packages and dependencies 
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
Open http://localhost:8080 to view it in the browser.

### Run tests
```
npm run test
```

### Compiles and minifies for production
```
npm run build
```

## To test CI/CD and auto deployment

### Trigger deploy previews on branches other than master

- Create a new branch
- Commit and push the new branch to this repository
- From the side menu, choose **CI/CD** > **Jobs** to see newest job
- When the CI finishes running, copy the URL from the line "Live Draft URL" to see the deploy previews.

### Deployment on master branches

Every merge being done on master branch will trigger CI/CD to run a new job with tag **--prod** to deploy on Netlify.

Live site is available at: https://restaurant-app-develop.netlify.com/

## Code snippets

### Get orders, display orders and total price
```javascript
import { mapGetters, mapActions } from "vuex";

export default {
  name: "Order",
  components: {
    ...
  },
  computed: {
    ...mapGetters(["allOrders", "totalPrice"]),
    ...mapActions(["countTotal"]),
    ordersLength() {
      return this.allOrders.length;
    }
  },

  created() {
    this.countTotal;
  },
  beforeUpdate() {
    this.countTotal;
  }
};

```

### Test component rendering, match snapshot and fire actions
```javascript
import { mount } from "@vue/test-utils";
import OrderItem from "../../OrderItem";
import store from "@/store";

describe("OrderItem", () => {
  const testItem = {
    id: 1,
    name: "test",
    price: 123,
    description: "description"
  };
  it("renders and matches snapshot", () => {
    const wrapper = mount(OrderItem, {
      propsData: {
        item: testItem
      },
      store
    });

    expect(wrapper.element).toMatchSnapshot();
    expect(wrapper.exists()).toBe(true);
  });

  it("has close button that fires action", () => {
    const closeMock = jest.fn();
    const wrapper = mount(OrderItem, {
      propsData: {
        item: testItem
      },
      store
    });

    wrapper.setMethods({ onClick: closeMock });

    const closeButton = wrapper.find(".order-item__remove");
    closeButton.trigger("click");
    expect(closeMock).toHaveBeenCalled();
  });
});

```
### Form submission
```javascript
import axios from "axios";

export default {
  name: "ReservationForm",
  components: {
    ...
  },
  data() {
    return {
      currentDate: new Date(),
      form: {
        email: "",
        date: "",
        time: "",
        guestNumber: 0,
        comment: ""
      },
      errMsg: null,
      successMsg: null
    };
  },
  methods: {
    onNumberInput(e) {
      this.form.guestNumber = e.target.value;
    },
    encode(data) {
      return Object.keys(data)
        .map(
          key => `${encodeURIComponent(key)}=${encodeURIComponent(data[key])}`
        )
        .join("&");
    },
    handleSubmit() {
      const axiosConfig = {
        header: { "Content-Type": "application/x-www-form-urlencoded" }
      };
      axios
        .post(
          "/reservation",
          this.encode({
            "form-name": "reservation",
            ...this.form
          }),
          axiosConfig
        )
        .then(() => {
          this.successMsg =
            "We have receive your reservation and will get to you soonest!";
          this.form = {};
        })
        .catch(() => {
          this.errMsg = "Something went wrong. Please try again";
        });
    }
  }
};
```

## Contributor

**Ca Duong** 
